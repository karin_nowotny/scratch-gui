// import VM from 'scratch-vm';
// import BugAnalyzerComponent from '../components/bug-analyzer/bug-analyzer.jsx';
import React from 'react';
import bindAll from 'lodash.bindall';
import BugAnalyzerComponent from '../components/bug-analyzer/bug-analyzer.jsx';
import PropTypes from "prop-types";
import VM from "scratch-vm";

class BugAnalyzer extends React.component {
    constructor (props){
        super(props);
        this.state = {
            blocks: null,
            errorMessage: 'an error occurred'
        };
        bindAll(this, [
            'blockListener'
        ]);
    }
    componentDidMount (){
        this.props.vm.blocks.addListener('BLOCKS', this.blockListener);
    }
    blockListener (blocks) {
        this.setState({blocks: blocks});
        console.log(this.props.vm.blocks);
    }
    render () {
        const {
            vm, // eslint-disable-line no-unused-vars
            ...props
        } = this.props;
        return (
            <BugAnalyzerComponent
                errorMessage={this.errorMessage}
                {...props}
            />
        );
    }

}

BugAnalyzer.propTypes = {
    vm: PropTypes.instanceOf(VM).isRequired
};
