import GUI from '../../containers/gui.jsx';
import React from 'react';
import {FormattedMessage} from 'react-intl';
import * as PropTypes from 'autoprefixer';

const BugAnalyzerComponent = props => {
    const {
        errorMessage
    } = props;

    return (
        <GUI {...props}>
            <FormattedMessage
                defaultMessage="The bug error message should be shown here."
                description="Text for testing position of the bug message."
                id="gui.bugAnalyzer.Text"
            />
            <span>{errorMessage}</span>
        </GUI>
    );
};

BugAnalyzerComponent.propTypes = {
    errorMessage: PropTypes.string
};

export default BugAnalyzerComponent;
